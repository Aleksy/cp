package cp.constants;

import cp.model.other.Size;

/**
 * Parametry
 */
public class Parameters {
   /**
    * Współczynnik uczenia
    */
   public static final double LEARNING_RATE = 0.1;
   /**
    * Rozmiar sieci Kohonena
    */
   public static final Size SOM_SIZE = Size.create(30, 30);
   /**
    * Liczba iteracji procesu uczenia
    */
   public static final int ITERATIONS = 10000;
}
