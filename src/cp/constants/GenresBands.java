package cp.constants;

import cp.model.data.Band;
import cp.model.data.Genre;
import cp.util.Bands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Klasa przechowująca mapę gatunek->zespoły i mapę zakodowaną
 */
public class GenresBands {
   /**
    * Mapa gatunek->zespoły
    */
   public static Map<Genre, List<Band>> GENRE_BANDS_MAP = init();
   /**
    * Zakodowana mapa z dwoma wektorami (wejściowy definiuje gatunek, wyjćiowy - listę zespołów)
    */
   public static Map<List<Double>, List<Double>> ENCODED_MAP = initEncoded();

   private static Map<List<Double>, List<Double>> initEncoded() {
      Map<List<Double>, List<Double>> map = new HashMap<>();
      List<Band> bands = new ArrayList<>();
      GENRE_BANDS_MAP.forEach((g, b) -> bands.addAll(b));
      List<Band> distinctBands = Bands.distinctBandList(bands);
      List<Genre> genres = new ArrayList<>(GENRE_BANDS_MAP.keySet());
      GENRE_BANDS_MAP.forEach((g, b) -> {
         List<Double> encodedGenres = new ArrayList<>();
         List<Double> encodedBands = new ArrayList<>();
         for (Genre genre : genres) {
            if (genre.getName().equals(g.getName())) {
               encodedGenres.add(1.0);
            } else {
               encodedGenres.add(0.0);
            }
         }
         for (Band distinctBand : distinctBands) {
            if (b.stream().anyMatch(band -> distinctBand.getName().equals(band.getName()))) {
               encodedBands.add(1.0);
            } else {
               encodedBands.add(0.0);
            }
         }
         map.put(encodedGenres, encodedBands);
      });
      return map;
   }

   private static Map<Genre, List<Band>> init() {
      Map<Genre, List<Band>> map = new HashMap<>();
      map.put(genre("Muzyka współczesna"), bands("Muzyka filmowa"));
      map.put(genre("Soft"), bands("Coldplay", "Elton John", "Keane"));
      map.put(genre("Chrześcijański"), bands("Thousand Foot Krutch", "Skillet", "Larry Norman"));
      map.put(genre("Metal"), bands("Slayer", "Metallica", "Judas Priest"));
      map.put(genre("Hard rock"), bands("The Jimi Hendrix Experience", "Alice Cooper", "AC/DC", "Kiss", "Aerosmith", "Queen"));
      map.put(genre("Punk"), bands("Ramones", "Television", "Blondie"));
      map.put(genre("Disco polo"), bands("Weekend", "Akcent", "Boys"));
      map.put(genre("Indie"), bands("Lykke Li", "Vampire Weekend", "Camera Obscura"));
      map.put(genre("Tradycyjny"), bands("Frank Sinatra", "Bing Crosby", "Nat King Cole"));
      map.put(genre("Rap"), bands("Eminem", "2Pac", "Dr. Dre"));
      map.put(genre("Blues"), bands("Jimi Hendrix", "The Rolling Stones", "Louis Armstrong"));
      map.put(genre("Soul"), bands("Ray Charles", "Otis Redding", "Al Green"));
      map.put(genre("Latin"), bands("Tito Puente", "Pancho Sanchez", "Eddie Palmieri"));
      map.put(genre("Swing"), bands("Django Reinhardt", "Benny Goodman", "Duke Ellington"));
      map.put(genre("Big band"), bands("Glenn Miller", "Big Bad Voodoo Daddy", "Michael Bublé"));
      map.put(genre("Reggae"), bands("Bob Marley", "Peter Tosh", "Dennis Brown"));
      map.put(genre("House"), bands("David Guetta", "Swedish House Mafia", "Roger Sanchez"));
      map.put(genre("Dubstep"), bands("Excision", "Skrillex", "Knife Party"));
      map.put(genre("Hard"), bands("Chris Liebing", "Adam Beyer", "DJ Rush", "Ruskie Techno"));
      map.put(genre("Trap"), bands("Young Igi", "Young Multi", "Kendrick Lamar"));
      map.put(genre("Uliczny"), bands("Peja", "Paktofonika"));
      map.put(genre("Gangsta"), bands("Ice Cube", "2Pac", "50Cent"));
      map.put(genre("C-pop"), bands("Super Junior M", "EXO-M", "Fahrenheit"));
      map.put(genre("K-pop"), bands("BlackPink", "BTS", "Twice"));
      map.put(genre("J-pop"), bands("AKB48", "Berryz Kobo", "Momoiro Clover Z"));
      return map;
   }

   private static List<Band> bands(String... names) {
      List<Band> bands = new ArrayList<>();
      for (int i = 0; i < names.length; i++) {
         bands.add(new Band(names[i]));
      }
      return bands;
   }

   private static Genre genre(String name) {
      return new Genre(name);
   }
}
