package cp.util;

import cp.model.neural.KohonenNeuron;
import cp.model.other.Range;
import cp.model.other.Size;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa użytkowa dla sieci neuronowych
 */
public class NeuralNetworks {
   /**
    * Tworzy mapę neuronów {@link KohonenNeuron} dla {@link cp.model.neural.Som}.
    * Przypisuje im współrzędne w mapie.
    *
    * @param size      rozmiar mapy
    * @param inputSize rozmiar neuronu wejściowego
    * @return neurony dla SOM
    */
   public static List<KohonenNeuron> createNeuralMapForSom(Size size, int inputSize) {
      List<KohonenNeuron> neurons = new ArrayList<>();
      for (int y = 0; y < size.y(); y++) {
         for (int x = 0; x < size.x(); x++) {
            neurons.add(KohonenNeuron.create(x, y, inputSize, Range.create(0.0, 1.0)));
         }
      }
      return neurons;
   }
}
