package cp.util;

import cp.model.data.Band;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa użytkowa dla zespołów muzycznych
 */
public class Bands {
   /**
    * Metoda ta zwraca listę zespołów o niepowtarzalnych nazwach
    * @param bands zespoły
    * @return lista niepowtarzalnych zespołów
    */
   public static List<Band> distinctBandList(List<Band> bands) {
      List<Band> distinct = new ArrayList<>();
      for (Band band : bands) {
         boolean alreadyAdded = false;
         for (Band b : distinct) {
            if (band.getName().equals(b.getName())) {
               alreadyAdded = true;
            }
         }
         if(!alreadyAdded) {
            distinct.add(band);
         }
      }
      return distinct;
   }
}
