package cp.util;

import java.util.List;

/**
 * Klasa użytkowa dla dystansów pomiędzy wektorami
 */
public class Distance {
   /**
    * Metoda oblicza dystans pomiędzy dwoma n-wymiarowymi wektorami
    *
    * @param v1 wektor 1
    * @param v2 wektor 2
    * @return dystans pomiędzy v1 i v2
    */
   public static double between(List<Double> v1, List<Double> v2) {
      Double d = 0.0;
      for (int i = 0; i < v2.size(); i++) {
         d += Math.pow(v1.get(i) - v2.get(i), 2);
      }
      return Math.sqrt(d);
   }
}