package cp.util;

import cp.model.other.Range;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Klasa użytkowa dla neuronów
 */
public class Neurons {
   /**
    * Metoda tworzy losowe wagi z zadanego zakresu.
    * @param size liczba wag do utworzenia
    * @param range zakres, z którego mają być losowane wartości wag
    * @return nowa lista wag
    */
   public static List<Double> createRandomWeights(int size, Range range) {
      Random random = new Random();
      List<Double> weights = new ArrayList<>();
      double from = range.from();
      double to = range.to();
      for (int i = 0; i < size; i++) {
         weights.add(from + (to - from) * random.nextDouble());
      }
      return weights;
   }
}
