package cp;

import cp.constants.Parameters;
import cp.model.data.Band;
import cp.model.data.Genre;
import cp.model.neural.CpNetwork;
import cp.util.Bands;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static cp.constants.GenresBands.ENCODED_MAP;
import static cp.constants.GenresBands.GENRE_BANDS_MAP;

/**
 * Klasa główna
 */
public class Main {
   /**
    * Metoda główna
    * @param args argumenty
    */
   public static void main(String[] args) {
      CpNetwork cpNetwork = CpNetwork.create(Parameters.SOM_SIZE, determineInputSize(), determineOutputSize());
      learn(cpNetwork);
      test(cpNetwork);
   }

   /**
    * Metoda testująca wytrenowaną sieć.
    * @param cpNetwork wytrenowana sieć.
    */
   private static void test(CpNetwork cpNetwork) {
      List<List<Double>> keys = new ArrayList<>(ENCODED_MAP.keySet());
      for (List<Double> key : keys) {
         List<Double> output = cpNetwork.f(key);
         print(key, output);
      }
   }

   /**
    * Metoda wypisująca wynik działania sieci.
    * @param input wektor wejściowy podany do sieci
    * @param output wektor wyjściowy uzyskany w odpowiedzi
    */
   private static void print(List<Double> input, List<Double> output) {
      List<Band> bnd = new ArrayList<>();
      GENRE_BANDS_MAP.forEach((g, b) -> bnd.addAll(b));
      List<Genre> genres = new ArrayList<>(GENRE_BANDS_MAP.keySet());
      List<Band> bands = Bands.distinctBandList(bnd);
      String genreName = null;
      List<String> bandsNames = new ArrayList<>();
      for(int i = 0; i < input.size(); i++) {
         if(input.get(i) > 0.5) {
            genreName = genres.get(i).getName();
         }
      }
      for(int i = 0; i < output.size(); i++) {
         if(output.get(i) > 0.5) {
            bandsNames.add(bands.get(i).getName());
         }
      }

      List<Band> bandsForGenre = GENRE_BANDS_MAP.get(new Genre(genreName));
      int matched = 0;
      for (Band band : bandsForGenre) {
         for (String bandName : bandsNames) {
            if(bandName.equals(band.getName())) {
               matched += 1;
            }
         }
      }

      for(int i = 0; i < input.size(); i++) {
         if(input.get(i) > 0.5) {
            System.out.print(genres.get(i).getName() + " ");
         }
      }
      System.out.println("[" + (((double)matched / (double)(bandsForGenre.size())) * 100) + "%]");
      System.out.print("        ");
      for(int i = 0; i < output.size(); i++) {
         if(output.get(i) > 0.5) {
            System.out.print(bands.get(i).getName() + " ");
         }
      }
      System.out.println();
   }

   /**
    * Metoda ucząca sieć neuronową
    * @param cpNetwork sieć do trenowania
    */
   private static void learn(CpNetwork cpNetwork) {
      Random random = new Random();
      List<List<Double>> keys = new ArrayList<>(ENCODED_MAP.keySet());
      for (int i = 0; i < Parameters.ITERATIONS; i++) {
         System.out.println("Iteration: " + i);
         List<Double> input = keys.get(random.nextInt(keys.size()));
         List<Double> output = ENCODED_MAP.get(input);
         cpNetwork.l(input, output, Parameters.LEARNING_RATE);
      }
   }

   private static int determineOutputSize() {
      List<Band> bands = new ArrayList<>();
      GENRE_BANDS_MAP.forEach((g, b) -> bands.addAll(b));
      return Bands.distinctBandList(bands).size();
   }

   private static int determineInputSize() {
      return GENRE_BANDS_MAP.size();
   }
}
