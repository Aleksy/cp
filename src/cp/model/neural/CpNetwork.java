package cp.model.neural;

import cp.model.other.Size;

import java.util.List;

/**
 * Sieć CP
 */
public class CpNetwork {
   private Som som;
   private GrossbergLayer gl;

   private CpNetwork(Size somSize, int inputSize, int grossbergLayerSize) {
      som = Som.create(somSize, inputSize);
      gl = GrossbergLayer.create(grossbergLayerSize, som.neurons().size());
   }

   /**
    * Główna funkcja całej sieci
    * @param input wektor wejściowy
    * @return wektor wyjściowy
    */
   public List<Double> f(List<Double> input) {
      List<Double> somOutput = som.f(input);
      return gl.f(somOutput);
   }

   /**
    * Funkcja wywołująca metody korygujące wagi neuronow
    * @param input wektor wejść
    * @param output wektor wyjść
    * @param learningRate współczynnik uczenia
    */
   public void l(List<Double> input, List<Double> output, double learningRate) {
      List<Double> somOutput = som.l(input, learningRate);
      gl.l(somOutput, output, learningRate);
   }

   /**
    * Funkcja tworząca nową sieć
    * @param somSize rozmiar warstwy Kohonena
    * @param inputSize rozmiar wektora wejściowego
    * @param grossbergLayerSize rozmiar wektora wyjściowego / liczba neuronów warstwy Grossberga
    * @return nowa instancja sieci CP
    */
   public static CpNetwork create(Size somSize, int inputSize, int grossbergLayerSize) {
      return new CpNetwork(somSize, inputSize, grossbergLayerSize);
   }
}
