package cp.model.neural;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstrakcyjny neuron
 */
public abstract class AbstractNeuron {
   /**
    * Wektor wag neuronu
    */
   protected List<Double> weights;

   /**
    * Konstruktor
    * @param weights wektor wag
    */
   AbstractNeuron(List<Double> weights) {
      this.weights = weights;
   }

   /**
    * Główna funkcja neuronu
    *
    * @param inputs do przetworzenia
    * @return wyjście neuronu
    */
   public Double f(List<Double> inputs) {
      Double sum = 0.0;
      for (int i = 0; i < inputs.size(); i++) {
         sum += weights.get(i) * inputs.get(i);
      }
      return axon(sum);
   }

   /**
    * Getter dla parametru weights
    *
    * @return weights
    */
   public List<Double> getWeights() {
      return new ArrayList<>(weights);
   }

   /**
    * Domyślna funkcja aksonowa. Do nadpisania w razie potrzeby.
    *
    * @param x do przetworzenia
    * @return wyjście funkcji
    */
   protected Double axon(Double x) {
      return x;
   }
}
