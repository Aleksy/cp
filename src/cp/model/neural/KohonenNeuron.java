package cp.model.neural;

import cp.model.other.Range;
import cp.util.Neurons;

import java.util.ArrayList;
import java.util.List;

/**
 * Neuron Kohonena
 */
public class KohonenNeuron extends AbstractNeuron {
   private int x;
   private int y;

   private KohonenNeuron(List<Double> weights, int x, int y) {
      super(weights);
      this.x = x;
      this.y = y;
   }

   /**
    * Getter dla parametru x
    *
    * @return x
    */
   public int x() {
      return x;
   }

   /**
    * Getter dla parametru y
    *
    * @return y
    */
   public int y() {
      return y;
   }

   /**
    * Getter dla parametru coordinates
    *
    * @return coordinates
    */
   public List<Double> coordinates() {
      List<Double> coordinates = new ArrayList<>();
      coordinates.add((double) x);
      coordinates.add((double) y);
      return coordinates;
   }

   /**
    * Metoda poprawiająca wagi neuronu
    * @param input wektor wejściowy
    * @param distance dystans pomiędzy punktem definiowanym przez wagi i punktem definiowanym przez wektor wejść
    * @param learningRate współczynnik uczenia
    */
   public void correct(List<Double> input, double distance, double learningRate) {
      List<Double> newWeights = new ArrayList<>();
      for (int i = 0; i < weights.size(); i++) {
         newWeights.add(weights.get(i) + learningRate * distance * (input.get(i) - weights.get(i)));
      }
      this.weights = newWeights;
   }

   /**
    * Tworzenie nowej instancji
    *
    * @param x     x
    * @param y     y
    * @param size  rozmiar
    * @param range zakres losowania wartości wag
    * @return nowy {@link KohonenNeuron}
    */
   public static KohonenNeuron create(int x, int y, int size, Range range) {
      return new KohonenNeuron(Neurons.createRandomWeights(size, range), x, y);
   }
}
