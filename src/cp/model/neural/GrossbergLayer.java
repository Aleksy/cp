package cp.model.neural;

import cp.model.other.Range;
import cp.model.other.Size;
import cp.util.NeuralNetworks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Warstwa Grossberga
 */
public class GrossbergLayer {
   private List<GrossbergNeuron> neurons;

   private GrossbergLayer(List<GrossbergNeuron> neurons) {
      this.neurons = neurons;
   }

   /**
    * Główna funkcja warstwy Grossberga
    * @param input wektor wejściowy
    * @return wektor wyjściowy
    */
   public List<Double> f(List<Double> input) {
      List<Double> output = new ArrayList<>();
      for (GrossbergNeuron neuron : neurons) {
         output.add(neuron.f(input));
      }
      return output;
   }

   /**
    * Funkcja wywołująca metody korygujące wagi neuronów.
    * @param input wektor wejść
    * @param output wektor wyjść
    * @param learningRate współczynnik uczenia
    */
   public void l(List<Double> input, List<Double> output, double learningRate) {
      int c = 0;
      for (GrossbergNeuron neuron : neurons) {
         neuron.correct(input, output.get(c++), learningRate);
      }
   }

   /**
    * Metoda tworząca nową instancję warstwy Grossberga.
    *
    * @param size      liczba neuronów
    * @param inputSize rozmiar wektora wejściowego, będącego wektorem wyjściowym warstwy Kohonena
    * @return nowa instancja
    */
   public static GrossbergLayer create(int size, int inputSize) {
      List<GrossbergNeuron> neurons = new ArrayList<>();
      for(int i = 0; i < size; i++) {
         neurons.add(GrossbergNeuron.create(inputSize, Range.create(0.0, 1.0)));
      }
      return new GrossbergLayer(neurons);
   }

}
