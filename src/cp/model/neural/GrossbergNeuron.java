package cp.model.neural;

import cp.model.other.Range;
import cp.util.Neurons;

import java.util.ArrayList;
import java.util.List;

/**
 * Neuron Grossberga
 */
public class GrossbergNeuron extends AbstractNeuron {
   /**
    * Konstruktor
    *
    * @param weights wektor wag
    */
   GrossbergNeuron(List<Double> weights) {
      super(weights);
   }

   /**
    * Metoda poprawiająca wagi neuronu
    *
    * @param input        wektor wejściowy
    * @param output       oczekiwana wartość wyjściowa
    * @param learningRate współczynnik uczenia
    */
   public void correct(List<Double> input, Double output, double learningRate) {
      List<Double> newWeights = new ArrayList<>();
      for (int i = 0; i < weights.size(); i++) {
         if (input.get(i) > 0.8) {
            newWeights.add(weights.get(i) + learningRate * (output - weights.get(i)));
         } else {
            newWeights.add(weights.get(i));
         }
      }
      this.weights = newWeights;
   }

   /**
    * Tworzenie nowej instancji.
    *
    * @param size  rozmiar neuronu
    * @param range zakres losowania wartości wag
    * @return nowy {@link GrossbergNeuron}
    */
   public static GrossbergNeuron create(int size, Range range) {
      return new GrossbergNeuron(Neurons.createRandomWeights(size, range));
   }
}
