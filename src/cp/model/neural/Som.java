package cp.model.neural;

import cp.model.other.Size;
import cp.util.Distance;
import cp.util.NeuralNetworks;

import java.util.ArrayList;
import java.util.List;

/**
 * Self Organising Map - Sieć Kohonena
 */
public class Som {
   private List<KohonenNeuron> neurons;

   private Som(List<KohonenNeuron> neurons) {
      this.neurons = neurons;
   }

   /**
    * Główna funkcja SOM.
    *
    * @param input wektor wejść
    * @return wektor wyjść
    */
   public List<Double> f(List<Double> input) {
      List<Double> output = new ArrayList<>();
      for (KohonenNeuron neuron : neurons) {
         output.add(neuron.f(input));
      }
      return mapOutputVectorToHotOne(output);
   }

   /**
    * Funkcja ucząca dla SOM.
    *
    * @param input wektor wejść
    * @return wektor wyjść
    */
   public List<Double> l(List<Double> input, double learningRate) {
      KohonenNeuron nearest = neurons.get(0);
      double bestDistanceToInputVector = Double.MAX_VALUE;
      for (KohonenNeuron neuron : neurons) {
         double distance = Distance.between(neuron.getWeights(), input);
         if (distance < bestDistanceToInputVector) {
            bestDistanceToInputVector = distance;
            nearest = neuron;
         }
      }
      for (KohonenNeuron neuron : neurons) {
         double distance = Distance.between(neuron.coordinates(), nearest.coordinates());
         if (distance < 0.5) {
            neuron.correct(input, Distance.between(neuron.getWeights(), input), learningRate);
         }
      }
      return f(input);
   }

   /**
    * Getter dla parametru neurons
    *
    * @return neurons
    */
   public List<KohonenNeuron> neurons() {
      return neurons;
   }

   /**
    * Tworzenie nowej SOM.
    *
    * @param size      rozmiar sieci
    * @param inputSize rozmiar wektora wejściowego
    * @return nowa {@link Som}
    */
   public static Som create(Size size, int inputSize) {
      return new Som(NeuralNetworks.createNeuralMapForSom(size, inputSize));
   }

   private List<Double> mapOutputVectorToHotOne(List<Double> output) {
      Double best = Double.MIN_VALUE;
      int bestIndex = -1;
      for (int i = 0; i < output.size(); i++) {
         if (output.get(i) > best) {
            best = output.get(i);
            bestIndex = i;
         }
      }
      List<Double> newOutput = new ArrayList<>();
      for (int i = 0; i < output.size(); i++) {
         if (i != bestIndex) {
            newOutput.add(0.0);
         } else {
            newOutput.add(1.0);
         }
      }
      return newOutput;
   }
}
