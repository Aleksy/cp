package cp.model.other;

/**
 * Rozmiar dwuwymiarowy
 */
public class Size {
   private int x;
   private int y;

   private Size(int x, int y) {
      this.x = x;
      this.y = y;
   }

   /**
    * Getter dla parametru x
    *
    * @return x
    */
   public int x() {
      return x;
   }

   /**
    * Getter dla parametru y
    *
    * @return y
    */
   public int y() {
      return y;
   }

   /**
    * Tworzenie nowej instancji
    * @param x rozmiar x
    * @param y rozmiar y
    * @return nowy {@link Size}
    */
   public static Size create(int x, int y) {
      return new Size(x, y);
   }
}
