package cp.model.other;

/**
 * Zakres pomiędzy dwoma liczbami
 */
public class Range {
   private double from;
   private double to;

   private Range(double from, double to) {
      this.from = from;
      this.to = to;
   }

   /**
    * Getter dla parametru from
    *
    * @return from
    */
   public double from() {
      return from;
   }

   /**
    * Getter dla parametru to
    *
    * @return to
    */
   public double to() {
      return to;
   }

   /**
    * Tworzenie nowej instancji
    * @param from od
    * @param to do
    * @return nowy {@link Range}
    */
   public static Range create(double from, double to) {
      return new Range(from, to);
   }
}
