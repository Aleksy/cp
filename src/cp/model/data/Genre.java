package cp.model.data;

import java.util.Objects;

/**
 * Gatunek muzyczny
 */
public class Genre {
   private String name;

   /**
    * Konstruktor
    * @param name nazwa gatunku
    */
   public Genre(String name) {
      this.name = name;
   }

   /**
    * Getter dla parametru name
    * @return name {@link String}
    */
   public String getName() {
      return name;
   }

   /**
    * Nadpisana metoda equals potrzebna po to by nazwa gatunku definiowała jego niepowtarzalność
    * @param o obiekt do porównania
    * @return efekt porównania
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Genre genre = (Genre) o;
      return Objects.equals(name, genre.name);
   }

   /**
    * Gatunki przechowywane są w {@link java.util.HashMap} więc muszą posiadać metodę hashującą.
    * @return efekt hashowania
    */
   @Override
   public int hashCode() {
      return Objects.hash(name);
   }
}
