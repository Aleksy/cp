package cp.model.data;

import java.util.Objects;

/**
 * Zespół muzyczny
 */
public class Band {
   private String name;

   /**
    * Konstruktor
    * @param name nazwa
    */
   public Band(String name) {
      this.name = name;
   }

   /**
    * Getter dla parametru name
    * @return name {@link String}
    */
   public String getName() {
      return name;
   }

   /**
    * Nadpisana metoda equals potrzebna po to by nazwa zespołu definiowała jego niepowtarzalność
    * @param o obiekt do porównania
    * @return efekt porównania
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Band band = (Band) o;
      return Objects.equals(name, band.name);
   }

   /**
    * Zespoły przechowywane są w {@link java.util.HashMap} więc muszą posiadać metodę hashującą.
    * @return efekt hashowania
    */
   @Override
   public int hashCode() {
      return Objects.hash(name);
   }
}
